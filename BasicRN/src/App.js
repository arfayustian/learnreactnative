import React, { Component, useState, useEffect } from "react";
import { ScrollView, Text, View } from "react-native";
import FieldProduct from "./pages/FieldProduct";
import BasicLayout from "./pages/BasicLayout";
import PositionRN from "./pages/PositionRN/PositionRN";
import PropsDinamis from "./pages/PropsDinamis";
import StateDinamis from "./pages/StateDinamis";
import Communication from "./pages/Communication/Communication";
import Cart from "./commponents/Cart";
import Product from "./commponents/Product";
import ReactNativeSvg from "./pages/ReactNativeSVG";

const App = () => {
  const [isShow, SetIsShow] = useState(true);
  useEffect (() => {
    setTimeout(() => {
      SetIsShow(false);
    }, 10000);
  }, [])
  return(
    <ScrollView>
      <View>
      {/* <PropsDinamis /> */}
      {/* {<BasicLayout />} */}
      {/* <StateDinamis /> */}
      {/* <Communication /> */}
      {/* <PositionRN /> */}
      {/* <FieldProduct /> */}
      <ReactNativeSvg />
    </View>
    </ScrollView>
  )
}

export default App;