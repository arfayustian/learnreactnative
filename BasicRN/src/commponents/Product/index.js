import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import Meja from "./../../assets/images/meja.jpg"

export default function Product(props) {
    return (
        <View>
                <View style={{width: 335, height: 360, backgroundColor: "lightgrey", borderRadius: 10, margin: 10}}>
                    <Image source={Meja} style={{borderRadius: 10, margin:15}}/>
                    <Text style={{marginLeft:15, fontWeight: "bold", fontSize: 16}}>Paket Belajar Sekolah Menengah Atas</Text>
                    <Text style={{fontSize: 16, fontWeight: "bold", marginLeft:15, color: "orange"}}>Rp. 1.000.000</Text>
                    <Text style={{marginLeft:15}}>Kabupaten Semarang</Text>
                    <TouchableOpacity onPress={props.onButtonPress}>
                    <View style={{marginLeft:15, marginTop:10, height:40, width:300, backgroundColor: "green", paddingVertical:10, borderRadius: 30}}>
                        <Text style={{textAlign:"center", fontWeight:"800", color:"white"}}>BELI</Text>
                    </View>
                    </TouchableOpacity>
                </View>
            </View>
    )
}

const styles = StyleSheet.create({})
