import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import shop from './../../assets/icons/shop.png'

export default function Cart(props) {
    return (
        <View>
             <View style={styles.wrapper}>
                <Text style={{margin:20}}>Materi Position</Text>
                <View style={styles.iconWrapper}>
                    <Image source={shop} style={styles.iconShop}/>
                    <Text style={styles.totalWishlist}>{props.jumlahBelanja}</Text>
                </View>
                <Text style={styles.nameIcon}> wishlist </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    iconShop: {
        height: 128,
        width: 128,
    },
    iconWrapper: {
        borderWidth: 3,
        height: 200,
        width:200,
        borderRadius:200/2,
        borderColor: 'green',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative'
        

    }, 
    wrapper:{
        alignItems: "center"
    },
    nameIcon:{
        fontSize:20,
        fontWeight:"bold",
        marginTop: 10
    },
    wrapperWishlist:{
        height: 30,
        width: 30,
        borderRadius: 30/2,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        right: 0

    },
    totalWishlist:{
        fontWeight: 'bold',
        color: 'white',
        fontSize: 20,
        backgroundColor: 'red',
        height:50,
        width:50,
        borderRadius: 50/2,
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 12
        
    }
})
