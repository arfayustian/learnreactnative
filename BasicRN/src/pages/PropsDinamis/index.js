import React from 'react'
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native'
import gedung from './../../assets/images/gedung.jpeg'

export default function PropsDinamis() {
    return (
        <View>
            <Text>Materi Dinamis Props</Text>
            <ScrollView horizontal>
            <View style={styles.fieldStory}>
            <Story judul='Langit' gambar={gedung}/>
            <Story judul='Gedung'/>
            <Story judul='Pohon'/>
            <Story judul='Lampu'/>
            <Story judul='Jalan'/>
            </View>
            </ScrollView>
        </View>
    )
}

const Story = (props) => {
    return(
        <View style={styles.wrapperStory}>
            <Image source={props.gambar} style={styles.gambarStory}/>
            <Text style={styles.judulStory}>{props.judul}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    gambarStory: {
        height:80,
        width: 80,
        borderRadius:80/2,
    },
    fieldStory: {
        alignItems: 'center',
        marginRight:10,
        flexDirection: 'row',
        margin: 5

    },
    judulStory: {
        textAlign: 'center'
    },
    wrapperStory: {
        marginRight: 5
    }
})
