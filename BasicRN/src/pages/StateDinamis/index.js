import React, {Component, useState} from 'react'
import { Button, StyleSheet, Text, View} from 'react-native'

export default function StateDinamis() {
    return (
        <View style={styles.wrapper}>
            <Text style={styles.textJudul}>Materi Component Dinamis dengan State</Text>
            <Text>Func Component (Hooks)</Text>
            <Counter />
            <Counter />
            <Text style={{marginTop:20}}>Class Component</Text>
            <CounterClass />
            <CounterClass />
        </View>
    )
}

const Counter = () => {
    const [number, setNumber] = useState(0)
    return(
        <View>
            <Text>{number}</Text>
            <Button title='Tambah' style={styles.styleButton} onPress={() => setNumber(number+1)}/>
        </View>
    )
}

class CounterClass extends Component {
    state ={
        number: 0,
    };
    render(){
        return(
            <View>
                <Text>{this.state.number}</Text>
                <Button title='Tambah' onPress={() => this.setState({number: this.state.number + 1})}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textJudul: {
        textAlign: 'center',
        margin: 5,
        fontSize: 17
    },
    styleButton: {

    },
    wrapper: {
        margin: 5
    }
})
