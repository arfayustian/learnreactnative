import { View, Text, Image } from "react-native";
import React, { Component, useState, setState, state, useEffect} from "react";

// class BasicLayout extends Component {
//     constructor(props){
//         super(props);
//             console.log('===> constructor')
//             this.state = {
//                 penghobi: 200,
//             }
//     }

//     componentDidMount(){
//         console.log('==>componentdidmount')
//         setTimeout(() => {
//         this.setState({
//             penghobi: 5000
//         })
//         }, 5000)
//     }

//     componentDidUpdate(){
//         console.log('==>componentDidUpdate')
//     }

//     componentWillUnmount(){
//         console.log('===>componentWillUnmount')
//     }

//     render(){
//         console.log('==>render')
//         return(
//             <View>
//                 <View style={{flexDirection: "row", justifyContent: "space-around"}}>
//                 <Text>Menu</Text>
//                 <Text>Search</Text>
//                 <Text>Setting</Text>
//                 <Text>About</Text>
//                 </View>
//                 <View style={{flexDirection: "row", margin:15, alignItems: "center"}}>
//                     <Image source={{uri:"https://placeimg.com/80/80/animals"}} style={{height:80, width:80, borderRadius:50, marginRight: 10}}/>
//                     <View>
//                         <Text style={{fontWeight: "bold"}}>Animal</Text>
//                         <Text>{this.state.penghobi} penghobi</Text>
//                     </View>
//                 </View>
//             </View>
            
//         )
//     }
// }

const BasicLayout = () => {
    const[subs, setSubs] = useState(200)
    useEffect(() => {
        console.log('did mount')
        setTimeout(() => {
            setSubs(400)
        }, 5000)
        return () => {
            console.log('did update')
        }
    }, [subs])
    return(
        <View>
                <View style={{flexDirection: "row", justifyContent: "space-around"}}>
                <Text>Menu</Text>
                <Text>Search</Text>
                <Text>Setting</Text>
                <Text>About</Text>
                </View>
                <View style={{flexDirection: "row", margin:15, alignItems: "center"}}>
                    <Image source={{uri:"https://placeimg.com/80/80/animals"}} style={{height:80, width:80, borderRadius:50, marginRight: 10}}/>
                    <View>
                        <Text style={{fontWeight: "bold"}}>Animal</Text>
                        <Text>{subs} penghobi</Text>
                    </View>
                </View>
        </View>
    )
}

export default BasicLayout;