import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Roket from './../../assets/ilustration/roket.svg'

export default function ReactNativeSvg() {
    return (
        <View>
            <Text>Materi SVG</Text>
            <Roket width={300} height={300}/>
        </View>
    )
}

const styles = StyleSheet.create({})
