import React, {useState} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Cart from '../../commponents/Cart'
import Product from '../../commponents/Product'

export default function Communication() {
    const [barang, setBarang] = useState(0)
    return (
        <View>
            <Text style={styles.judul}>Materi Communication Component</Text>
            <Cart jumlahBelanja={barang}/>
            <Product onButtonPress={()=> setBarang(barang+1)}/>
        </View>
    )
}

const styles = StyleSheet.create({
    judul: {
        margin:10,
        fontSize: 18,
        textAlign: 'center'
    }
})
